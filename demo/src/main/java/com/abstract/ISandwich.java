public interface ISandwich {
    String getFilling();
    void addFiling(String topping);
    boolean isVegeterian();
}